//
//  ListNotesViewController.swift
//  CloudKitExample
//
//  Created by KMSOFT on 08/03/17.
//
//

import UIKit
import CloudKit
class ListNotesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblNotes: UITableView!
    var arrNotes: Array<CKRecord> = []
    
    var selectedNoteIndex: Int!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblNotes.delegate = self
        tblNotes.dataSource = self
        tblNotes.hidden = true
        
        fetchNotes()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrNotes.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ListNotesViewController", forIndexPath: indexPath)
        let noteRecord: CKRecord = arrNotes[indexPath.row]
        cell.textLabel?.text = noteRecord.valueForKey("NoteTitle") as? String
        
        return cell

    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 80.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedNoteIndex = indexPath.row
        //performSegueWithIdentifier("idSegueEditNote", sender: self)
        print(indexPath.row)
        
    }
    
    // MARK: Custom method implementation
    
    func fetchNotes() {
        let container = CKContainer.defaultContainer()
        let privateDatabase = container.privateCloudDatabase
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "Establishment", predicate: predicate)
        
        privateDatabase.performQuery(query, inZoneWithID: nil) { (results, error) -> Void in
            if error != nil {
                print(error)
            }
            else {
                print(results)
                
                for result in results! {
                    //self.arrNotes.append(result as! CKRecord)
                    self.arrNotes.append(result as CKRecord)
                    
                }
                
                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                    self.tblNotes.reloadData()
                    self.tblNotes.hidden = false
                })
            }
        }
    }

    
}
