//
//  ViewController.swift
//  CloudKitExample
//
//  Created by KMSOFT on 08/03/17.
//
//

import UIKit
import QuartzCore
import CloudKit



protocol EditNoteViewControllerDelegate {
    func didSaveNote(noteRecord: CKRecord, wasEditingNote: Bool)
}

class ViewController: UIViewController {

    
    @IBOutlet weak var TitleTextBox: UITextField!
    @IBOutlet weak var DetailsTextBox: UITextView!
   var editedNoteRecord: CKRecord!
    
    @IBAction func SaveNoteButton(sender: AnyObject) {
        
        var noteRecord: CKRecord
        var isEditingNote : Bool
        
        if let editedNote = editedNoteRecord{
            noteRecord = editedNote
            isEditingNote = true
        }  else {
            let timestampAsString = String(format: "%f", NSDate.timeIntervalSinceReferenceDate())
            let timestampParts = timestampAsString.componentsSeparatedByString(".")
            let noteID = CKRecordID(recordName: timestampParts[0])
            
            noteRecord = CKRecord(recordType: "Notes", recordID: noteID)
            
            isEditingNote = false
        }
        noteRecord.setObject(TitleTextBox.text, forKey: "NoteTitle")
        noteRecord.setObject(DetailsTextBox.text, forKey: "NoteDetails")
        
        let container = CKContainer.defaultContainer()
        let privateDatabase = container.privateCloudDatabase
        
        privateDatabase.saveRecord(noteRecord) { (record, error) -> Void in
            if (error != nil) {
                print(error)
            }
            else {
                //self.delegate.didSaveNote(noteRecord, wasEditingNote: isEditingNote)
                
                
            }
            
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
               // self.viewWait.hidden = true
                self.navigationController?.setNavigationBarHidden(false, animated: true)
            })
        }
    }
    
    func handleSwipeDownGestureRecognizer(swipeGestureRecognizer: UISwipeGestureRecognizer) {
        TitleTextBox.resignFirstResponder()
        DetailsTextBox.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

